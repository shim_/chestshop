/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Acrobot.ChestShop.Listeners.Economy;

import com.Acrobot.ChestShop.ChestShop;
import com.Acrobot.ChestShop.Events.Economy.CurrencyAddEvent;
import com.Acrobot.ChestShop.Events.Economy.CurrencyAmountEvent;
import com.Acrobot.ChestShop.Events.Economy.CurrencyCheckEvent;
import com.Acrobot.ChestShop.Events.Economy.CurrencySubtractEvent;
import java.math.BigDecimal;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

/**
 *
 * @author marvin
 */
public class CacheTransactions implements Listener {

    private final ConcurrentHashMap<String, BigDecimal> pendingTransactions = new ConcurrentHashMap<String, BigDecimal>();
    
    private final ConcurrentHashMap<String, BigDecimal> balances = new ConcurrentHashMap<String, BigDecimal>();

    private final CopyOnWriteArrayList<String> currentFlushes = new CopyOnWriteArrayList<String>();

    public BigDecimal getChange(String player) {
        if (pendingTransactions.containsKey(player)) {
            return pendingTransactions.get(player);
        }
        return BigDecimal.ZERO;
    }

    public void addChange(String player, BigDecimal change) {
        pendingTransactions.put(player, getChange(player).add(change));
    }

    public void flushTransactions() {
        for (Map.Entry<String, BigDecimal> entry : pendingTransactions.entrySet()) {
            String account = entry.getKey();
            BigDecimal change = entry.getValue();
            Player player = Bukkit.getPlayerExact(account);
            World world = Bukkit.getServer().getWorlds().get(0);
            if (player != null) {
                world = player.getWorld();
            }
            currentFlushes.add(account);
            if (change.compareTo(BigDecimal.ZERO) > 0) {
                //Add
                ChestShop.callEvent(new CurrencyAddEvent(change, account, world));
            } else {
                //Substract change * -1
                ChestShop.callEvent(new CurrencySubtractEvent(change.multiply(BigDecimal.valueOf(-1)), account, world));
            }
            ChestShop.getBukkitLogger().log(Level.INFO, String.format("Flush: Account: %s, Change: %s", account, change.toString()));
            pendingTransactions.put(account, BigDecimal.ZERO);

        }
        pendingTransactions.clear();
        balances.clear();
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onCurrencyAdd(CurrencyAddEvent event) {
        if (currentFlushes.contains(event.getTarget())) {
            currentFlushes.remove(event.getTarget());
            return;
        }
        addChange(event.getTarget(), event.getAmount());
        event.setAdded(true);
        event.setAmount(BigDecimal.ZERO);
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onCurrencySubtract(CurrencySubtractEvent event) {
        if (currentFlushes.contains(event.getTarget())) {
            currentFlushes.remove(event.getTarget());
            return;
        }
        addChange(event.getTarget(), event.getAmount().multiply(BigDecimal.valueOf(-1)));
        event.setSubtracted(true);
        event.setAmount(BigDecimal.ZERO);
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onAmountCheck(CurrencyAmountEvent event) {
        event.setAmount(getChange(event.getAccount()).add(event.getAmount()));
    }
    
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onAmountCheckCompleted(CurrencyAmountEvent event) {
        balances.put(event.getAccount(), event.getAmount());
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onCurrencyCheck(CurrencyCheckEvent event) {
        event.setAmount(event.getAmount().add(getChange(event.getAccount()).multiply(BigDecimal.valueOf(-1))));
        if(balances.get(event.getAccount()) == null && balances.get(event.getAccount()) != BigDecimal.ZERO){
            CurrencyAmountEvent amountEvent = new CurrencyAmountEvent(event.getAccount(), event.getWorld());
            Bukkit.getServer().getPluginManager().callEvent(amountEvent);
            balances.put(event.getAccount(), amountEvent.getAmount());
        }
        event.hasEnough(balances.get(event.getAccount()).compareTo(event.getAmount()) > 0);
        
    }
}
